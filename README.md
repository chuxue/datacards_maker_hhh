# datacards_maker_HHH

## Setting up

The scripts for datacard making are based in combine Havester.
To set it up follow the instructions [here](https://cms-analysis.github.io/CombineHarvester/)

To our convenience they also go bellow.
No specific tag is important as we are not using recent features of it.

```
export SCRAM_ARCH=slc6_amd64_gcc530
scram project CMSSW CMSSW_10_2_13
cd CMSSW_10_2_13/src
cmsenv
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester
scram b
```

## Making datacards

### Step 1: Formatting the input

PS.: For just this step, there is no need to setup CMSSW/combine/CombineHarvester, just plain pyROOT

  - We need one file per category with histograms, so in this step we just rearrange the root files as provided
    - Any renaming of processes can be done in this step

The example bellow is given with working paths to input files

```
python scripts/format_root.py \
 --inputShapes /afs/cern.ch/work/m/mstamenk/public/forXanda/histo-root-all-HLT-BDTCUT-v20-inclusive-loose-wp-CAT-ERA/ \
 --outputFolder prepare_cards \
 --channel HHH_6b \
 --era 2017 \
 --category 0ptag,5tag,6tag \
 --histogram bdt
```

The procedure is not fully automatic yet, the processes list is hrdcoded in this file.
Better organization pending some synergy with the inputs format.

@Marko: do we live this step here or adapt the output of the framework?
It should be out of the box to you run and get a local example of the result files in `prepare_cards`.
- In thinking consider how you would like to implement shape systematics. To this repository work with minimal changes it should be extra histograms in the same file as `process_systematicsNAmeUp/Down`

### Step 2: Preparing the datacard

  - The list of systematics is to be implemented [here](https://gitlab.cern.ch/acarvalh/datacards_maker_hhh/-/blob/master/configs/list_syst.py) -- to be expanded
  - The list of categories (now only one) and proceses per category is [here](https://gitlab.cern.ch/acarvalh/datacards_maker_hhh/-/blob/master/configs/list_channels.py)
    - Right now you need to do it category by category, we can automatize that to a list
    - We could use this place to make cards to all cases (resonant & non-resonant) just adding channels options to those configs
  - To produce the datacard to one of the categories implemented on the ata file above do:

```
python scripts/WriteDatacards.py \
--inputShapes prepare_cards/prepareDatacards_6tag_2017.root \
--channel 6tag  \
--cardFolder teste_datacards \
--no_data \
--noX_prefix
```

The formatted txt/root files will appear in a subfolder `teste_datacards`

- The "_mod" in the output file refers to modifications on the shape systematics, a relic from annother analysis that it is still not clear if we will need here, so I am keeping for now

### Step 3: testing the card

PS.: For just this step, there is no need of CombineHarvester package, only to setup combine

- Those are dirty scripts for a quick test
  - The computations, visualization and parameter scans are to be integrated in inference tool  toguether with the HH analyses (also for the resonant case)
    - and at some point I will provide that recipe instead. Maybe after having the cards that work with the physics model for the non-resonant case.

```
mkdir combine_results
cd combine_results
```

- If there are several categories they can be combined at datacard level in a command line at this point

create workspace

```
text2workspace.py \
../teste_datacards/datacard_6tag_2017_mod.txt  \
--out workspace.root  \
--PO 'map=.*/GluGluToHHH.*:r[1,-2000,2000]'
```


and compute assymptotic limis (`-t -1` means doing the Asimov/blinded)

```
combine -t -1 workspace.root
```

If you want to run the limit without `autoMCstats` remove the last line of the txt file (there is a combine comand line option to ignore it, but I don't remember now)

#### Doing pre/postfit plots

Do the file with the pre/postfit shapes using combine (do it in the same folder as above).
The commands bellow have options to do prefit.

```
combine -M FitDiagnostics \
-t -1 \
workspace.root \
--saveShapes \
--saveWithUncertainties \
--saveNormalization
```

Plot the shapes

```
cd ..

mkdir combine_results/test_plots/

python scripts/postfit_plots.py \
--plot_options_dict plot_configs/plot_options.json \
--output_folder combine_results/test_plots/ \
--overwrite_fitdiag combine_results/fitDiagnosticsTest.root
```

- The plotting options are in the configuration file given in the command line (to extend to other channels).
  - In this configurato file, you need to give the original file if you want the X-axis to be normalized as the input (try to do without, replave the path to "none" to see the effect)
