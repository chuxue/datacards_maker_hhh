import ROOT
import shutil
import sys, os, re, shlex
from subprocess import Popen, PIPE

import os.path
from os import path


from optparse import OptionParser
parser = OptionParser()
parser.add_option("--inputShapes",    type="string",       dest="inputShapes", help="Full path of prepareDatacards.root")
parser.add_option("--channel",        type="string",       dest="channel",     help="Channel to assume (to get the correct set of processes and syst) -- Noy yet implemented, right now just for naming purposes")
parser.add_option("--outputFolder",   type="string",       dest="outputFolder",  help="Folder where to save the root file to prepare the datacards (relative or full).\n Default: prepare_cards",  default="prepare_cards")
parser.add_option("--category",       type="string",       dest="category",    help="For naming purposes. If the input path is given with the string CAT that will replace the string. Can be several, separated by virgula, it will then make a loop.", default="0ptag,5tag,6tag")
parser.add_option("--histogram",      type="string",       dest="histogram",   help="Histogram to take from the input file.\n Default: bdt", default="bdt")
parser.add_option("--era",            type="string",       dest="era",         help="If the input path is given with the string ERA that will replace the string. If 0 is given it will loop on 2016,207,2018 (IMPLEMENT). It will anso apper as postfix of the output file. \n Default: ", default="")
parser.add_option("--shapeSyst",      action="store_true", dest="shapeSyst",   help="Copy the shape systematics histograms. Not implemented. Default: False", default=False)

(options, args) = parser.parse_args()

inputShapes  = options.inputShapes
channel      = options.channel
outputFolder = options.outputFolder
category     = options.category.split(",")
histogram    = options.histogram
era          = options.era
shape        = options.shapeSyst

# take procesess from config given the channel
processes = ["GluGluToHHHTo6B_SM", "JetHT", "QCD", "TT", "WJetsToQQ", "WWTo4Q", "WWW", "WWZ", "WZZ", "ZJetsToQQ", "ZZTo4Q", "ZZZ"]

# output the prepare cards
outputFolder = options.outputFolder
if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)

for cat in category :

    file_out_name = "%s/prepareDatacards_%s.root" % (outputFolder, cat) if era == "none" else "%s/prepareDatacards_%s_%s.root" % (outputFolder, cat, era)
    print("Creating %s" % file_out_name)
    fileOut  = ROOT.TFile(file_out_name, "recreate")

    file_in_name = inputShapes.replace("CAT",cat).replace("ERA",era)

    for pp, proc in enumerate(processes) :
        file = file_in_name + "/histograms_" + proc + ".root"
        print(proc)
        fin           = ROOT.TFile(file) #("%s/%s" % (odir, fileDatacard))
        bdt_histogram = fin.Get(histogram)
        proc_name = proc
        # do a json with replacements
        if proc_name == "JetHT" :
            proc_name = "data_obs"

        #bdt_histogram_renamed = ROOT.TH1D()
        bdt_histogram_renamed = bdt_histogram.Clone()
        bdt_histogram_renamed.SetName(proc_name)
        #type(obj)

        print(bdt_histogram.Integral(), bdt_histogram_renamed.Integral())
        #print(type(bdt_histogram), type(bdt_histogram_renamed))

        fileOut.cd()
        bdt_histogram_renamed.Write()
    print("Wrote %s" % file_out_name)

    #for hist in histograms :
    #    histogram.Write("",TObject.kOverwrite)
    #    histo.Write("",TObject.kOverwrite)
    #    print ("histo.Write("",TObject.kOverwrite) :: histoName: ",histo.GetName())

"""
Add the follohing renamings for the BRs uncertainties application
GluGluToHHHTo6B_SM --> GluGluToHHH_SM_bbbbbb

"""
