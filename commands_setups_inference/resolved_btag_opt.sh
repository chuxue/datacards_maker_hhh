export DHI_SCHEDULER_HOST="hh:cmshhcombr2@hh-scheduler2.cern.ch"
export DHI_HOOK_FILE="$DHI_BASE/dhi/hooks/run2_combination.py"
#export IMPACT_LABELS="/eos/user/a/acarvalh/HH_fullRun2_fits/misc/nuisance_labels.py"
export DHI_STORE="/eos/home-a/acarvalh/HHH/tasks_output"
export DHI_STORE_JOBS="$DHI_BASE/../jobs/"

#this path is for bdt cut 0.1 5L1M combine  
# /eos/user/x/xgeng/workspace/HHH/CMSSW_11_3_4/src/datacards_maker_hhh/teste_datacards/divide_bdt_cut_0.1/2017/histograms_combine_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt

#this path is for bdt cut 0.1 5L1M not combine 
# /eos/user/x/xgeng/workspace/HHH/CMSSW_11_3_4/src/datacards_maker_hhh/teste_datacards/divide_bdt_cut_0.1/2017/histograms_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt

#export CreateWSArgs=" --for-fits --no-wrappers --optimize-simpdf-constraints cms --X-pack-asympows --X-optimizeMHDependency=fixed --optimize-simpdf-constraints=cms --use-histsum --physics-option doBRU=True " # " "
#export CombineArgs="   --noMCbonly 1   --cminApproxPreFitTolerance=100   --X-rtd MINIMIZER_MaxCalls=9999999  --X-rtd MINIMIZER_analytic  --X-rtd FAST_VERTICAL_MORPH  --X-rtd OPTIMIZE_BOUNDS=0  --X-rtd SIMNLL_GROUPCONSTRAINTS=10  --X-rtd CACHINGPDF_NOCLONE    " # --saveInactivePOI 1 --generateBinnedWorkaround

# Cards from Xinyue
export DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017="/eos/user/x/xgeng/workspace/HHH/CMSSW_11_3_4/src/datacards_maker_hhh/teste_datacards/divide_bdt_cut_0.1/2017/"
# Cards from Marko
export DHI_DATACARDS_RUN2_boosted=/eos/user/m/mstamenk/CxAOD31run/hhh-6b/datacards/datacards_2016APV201620172018_v26_1PFfatgt1PFfatmvaResolved

export HHH_SM_inclusive=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt

export HHH_resolved_SM_6M=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_6M_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt
export HHH_resolved_SM_5M=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_5M_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt
export HHH_resolved_SM_4M=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_4M_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt
export HHH_resolved_SM_3M=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_3M_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt
export HHH_resolved_SM_2M=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_2M_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt
export HHH_resolved_SM_2M=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_1M_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt

#export HHH_resolved_SM_combined=$HHH_SM_6M,$HHH_SM_5M,$HHH_SM_4M,$HHH_SM_3M,$HHH_SM_2M,$HHH_SM_1M
export HHH_resolved_SM_combined=$DHI_DATACARDS_divide_bdt_cut_0o1_resolved_2017/histograms_combine_h1_t3_mass_mod_gt5bloose_gt0medium_0PFfat_.txt.txt

export       HHH_resolved_SM_comparison=$HHH_SM_6M:$HHH_SM_5M:$HHH_SM_4M:$HHH_SM_3M:$HHH_SM_2M:$HHH_SM_1M:$HHH_resolved_SM_combined:$HHH_SM_inclusive
export HHH_resolved_SM_comparison_names=HHH_SM_6M,HHH_SM_5M,HHH_SM_4M,HHH_SM_3M,HHH_SM_2M,HHH_SM_1M,HHH_resolved_SM_combined,HHH_SM_inclusive

#export HHH_SM_combo_mva_fit_resolved=$DHI_DATACARDS_RUN2_boosted/histograms_2016APV201620172018.txt

export HHH_boosted_SM_fatJet1Mass1_cat1=$DHI_DATACARDS_RUN2_boosted/histograms_fatJet1Mass1PFfat_cat1_SR_mod.txt
export HHH_boosted_SM_fatJet1Mass1_cat2=$DHI_DATACARDS_RUN2_boosted/histograms_fatJet1Mass1PFfat_cat2_SR_mod.txt
export HHH_boosted_SM_fatJet1Mass1_cat3=$DHI_DATACARDS_RUN2_boosted/histograms_fatJet1Mass1PFfat_cat3_SR_mod.txt
export HHH_boosted_SM_fatJet1Massgt1_cat1=$DHI_DATACARDS_RUN2_boosted/histograms_fatJet1Massgt1PFfat_cat1_SR_mod.txt
export HHH_boosted_SM_fatJet1Massgt1_cat2=$DHI_DATACARDS_RUN2_boosted/histograms_fatJet1Massgt1PFfat_cat2_SR_mod.txt
export HHH_boosted_SM_fatJet1Massgt1_cat3=$DHI_DATACARDS_RUN2_boosted/histograms_fatJet1Massgt1PFfat_cat3_SR_mod.txt
export HHH_boosted_combo_SM=$DHI_DATACARDS_RUN2_boosted/histograms_boosted_2016APV201620172018.txt

#export HHH_boosted_SM_resolved_6L_mva_fit=$DHI_DATACARDS_RUN2_boosted/histograms_mva6bmva6b_SR_mod.txt
export HHH_boosted_SM_resolved_5L_mva_fit=$DHI_DATACARDS_RUN2_boosted/histograms_mvamva5b_SR_mod.txt
export HHH_boosted_SM_resolved_6L_mva_fit=$DHI_DATACARDS_RUN2_boosted/histograms_mvamva6b_SR_mod.txt
#export HHH_boosted_SM=$DHI_DATACARDS_RUN2_boosted/histograms_resolved_2016APV201620172018.txt

export       HHH_boosted_SM_comparison=$HHH_boosted_SM_fatJet1Mass1_cat1:$HHH_boosted_SM_fatJet1Mass1_cat2:$HHH_boosted_SM_fatJet1Mass1_cat3:$HHH_boosted_SM_fatJet1Massgt1_cat1:$HHH_boosted_SM_fatJet1Massgt1_cat2:$HHH_boosted_SM_fatJet1Massgt1_cat3:$HHH_boosted_combo_SM
export HHH_boosted_SM_comparison_names=fatJet1Mass1_cat1:fatJet1Mass1_cat2:fatJet1Mass1_cat3:fatJet1Massgt1_cat1:fatJet1Massgt1_cat2:fatJet1Massgt1_cat3:boosted_combo_SM



#export       HHH_SM_coombo= get the best resolved to combine with 

