def list_channels() :

    #sigs = [["signal_ggf_nonresonant_cHHH1_hh_bbtt", "signal_ggf_nonresonant_cHHH1_hh_bbvv_sl", "signal_ggf_nonresonant_cHHH1_hh_bbvv"]]
    #sigs = [["signal_ggf_nonresonant_hh_bbttSM", "signal_ggf_nonresonant_hh_bbvv_slSM", "signal_ggf_nonresonant_hh_bbvvSM" ]]
    #sigs = [["signal_ggf_nonresonant_hh_bbttBM12", "signal_ggf_nonresonant_hh_bbvv_slBM12", "signal_ggf_nonresonant_hh_bbvvBM12" ]]
    sigs = [["GluGluToHHHTo6B_SM"]]
    #sigs = [["signal_ggf_nonresonant_hh_bbttkl_1p00", "signal_ggf_nonresonant_hh_bbvv_slkl_1p00", "signal_ggf_nonresonant_hh_bbvvkl_1p00"]]
    higgs_procs = sigs
    conversions = "Convs"
    #print (higgs_procs)

    info_channel = {
    "0ptag" : {
        "bkg_proc_from_data" : [  ],
        "bkg_procs_from_MC"  : ["QCD", "TT", "WJetsToQQ", "WWTo4Q", "WWW", "WWZ", "WZZ", "ZJetsToQQ", "ZZTo4Q", "ZZZ"],
        "isSMCSplit" : False,
        "proc_to_remove" : {}
    },
    "5tag" : {
        "bkg_proc_from_data" : [  ],
        "bkg_procs_from_MC"  : ["QCD", "TT", "WJetsToQQ", "WWTo4Q", "WWW", "WWZ", "WZZ", "ZJetsToQQ", "ZZTo4Q", "ZZZ"],
        "isSMCSplit" : False,
        "proc_to_remove" : {}
    },
    "6tag" : {
        "bkg_proc_from_data" : [  ],
        "bkg_procs_from_MC"  : ["QCD", "TT", "WJetsToQQ", "WWTo4Q", "WWW", "WWZ", "WZZ", "ZJetsToQQ", "ZZTo4Q", "ZZZ"],
        "isSMCSplit" : False,
        "proc_to_remove" : {}
    }
    }
    decays = []
    decays_hh = []

    return {
        "higgs_procs"      : higgs_procs,
        "decays"           : decays,
        "decays_hh"        : decays_hh,
        "info_bkg_channel" : info_channel
    }
