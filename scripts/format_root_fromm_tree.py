import ROOT
import shutil
import sys, os, re, shlex
import shutil,subprocess
from root_numpy import tree2array, fill_hist
import os.path
from os import path
import pandas
import time
from numpy import loadtxt

import sklearn
from sklearn.externals import joblib
print('The scikit-learn version is {}.'.format(sklearn.__version__))
import cPickle as pickle
print('The pickle version is {}.'.format(pickle.__version__))
import numpy as np
print('The numpy version is {}.'.format(np.__version__))


from optparse import OptionParser
parser = OptionParser()
parser.add_option("--inputShapes",    type="string",       dest="inputShapes", help="Full path of prepareDatacards.root")
parser.add_option("--channel",        type="string",       dest="channel",     help="Channel to assume (to get the correct set of processes and syst) -- Noy yet implemented, right now just for naming purposes")
parser.add_option("--outputFolder",   type="string",       dest="outputFolder",  help="Folder where to save the root file to prepare the datacards (relative or full).\n Default: prepare_cards",  default="prepare_cards")
parser.add_option("--category",       type="string",       dest="category",    help="For naming purposes. If the input path is given with the string CAT that will replace the string. Can be several, separated by virgula, it will then make a loop.", default="0ptag,5tag,6tag")
parser.add_option("--histogram",      type="string",       dest="histogram",   help="Histogram to take from the input file.\n Default: bdt", default="bdt")
parser.add_option("--era",            type="string",       dest="era",         help="If the input path is given with the string ERA that will replace the string. If 0 is given it will loop on 2016,207,2018 (IMPLEMENT). It will anso apper as postfix of the output file. \n Default: ", default="")
parser.add_option("--shapeSyst",      action="store_true", dest="shapeSyst",   help="Copy the shape systematics histograms. Not implemented. Default: False", default=False)

(options, args) = parser.parse_args()

inputShapesOriginal= '/eos/user/m/mstamenk/CxAOD31run/hhh-6b/mva-inputs-v24/mva-inputs-HLT-fit-inputs-all-fixes-v24-inclusive-loose-wp-0ptag-2017' #'/eos/user/m/mstamenk/CxAOD31run/hhh-6b/bdt-inputs/samples-v20-2017-nanoaod/'
inputShapes  = "/eos/user/a/acarvalh/HHH/skimmed_trees/" #options.inputShapes
channel      = "HHH_6b" #options.channel
outputFolder = "/eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees/" #"prepare_cards_from_root" #options.outputFolder
categories     = [
    #"1fat_gt2tag",
    #"2fat_gt2tag",
    #"gt2fat_gt2tag",
    #"0fat_gt4tag",
    #"gt0fat_gt2tag",
    #"final_selection_jetMultiplicity",
    "6loose"
]#options.category.split(",")

bdts_base_dir = "/eos/user/a/acarvalh/HHH/BDT_results/"
histograms    = {
    #"TMVA_bdt"                         : "bdt", # doing with jet selection / done without HHH_6b_TMVA_bdt/datacard_6loose_2017_mod.txt
    # /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_TMVA_bdt/datacard_final_selection_jetMultiplicity_2017_mod.txt
    # /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_TMVA_bdt/datacard_6loose_2017_mod.txt
    #"HT_6bloose"                       : "ht",
    #"XGBoost_6bloose"                   : "HHH_6b_withAllBtag/HHH_6b_XGB_withAllBtag_to_6loose_11Var", # to without jet selection
    #"XGBoost_jetMultSel"               : {
    #    "fit_var" : "HHH_6b_noFatJetVar/HHH_6b_XGB_noFatJetVar_final_selection_jetMultiplicity_10Var",
    #    "binning" : [30, 0, 1]
    #    },
    #"XGBoost_withFatJetVar_allBKG"               : {
    #    "fit_var" : "HHH_6b_withFatJetVar_allBKG/HHH_6b_XGB_withFatJetVar_allBKG_final_selection_jetMultiplicity_18Var",
    #    "binning" : [30, 0, 1]
    #    },
    #    # gt2fat_gt2tag: /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_withFatJetVar_allBKG/datacard_gt2fat_gt2tag_2017_mod.txt
    #    # 2fat_gt2tag:   /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_withFatJetVar_allBKG/datacard_2fat_gt2tag_2017_mod.txt
    #    # 1fat_gt2tag:   /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_withFatJetVar_allBKG/datacard_1fat_gt2tag_2017_mod.txt
    #    # 0fat_gt4tag:   /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_withFatJetVar_allBKG/datacard_0fat_gt4tag_2017_mod.txt
    #hadd prepareDatacards_2017.root prepareDatacards_0fat_gt4tag_2017.root  prepareDatacards_2fat_gt2tag_2017.root    prepareDatacards_gt2fat_gt2tag_2017.root prepareDatacards_1fat_gt2tag_2017.root
    #hadd prepareDatacards_gt0fat_gt2tag_2017.root prepareDatacards_2fat_gt2tag_2017.root    prepareDatacards_gt2fat_gt2tag_2017.root prepareDatacards_1fat_gt2tag_2017.root
    # /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees/HHH_6b_XGBoost_withFatJetVar_allBKG//datacard_2017_mod.txt
    #"XGBoost_noFatJetVar_allBKG"               : {
    #    "fit_var" : "HHH_6b_noFatJetVar_allBKG/HHH_6b_XGB_onlyUnderstood_final_selection_jetMultiplicity_12Var",
    #    "binning" : [30, 0, 1]
    #    },
    #    # gt2fat_gt2tag:  /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_noFatJetVar_allBKG/datacard_gt2fat_gt2tag_2017_mod.txt
    #    # 2fat_gt2tag: /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_noFatJetVar_allBKG/datacard_2fat_gt2tag_2017_mod.txt
    #    # 1fat_gt2tag: /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_noFatJetVar_allBKG/datacard_1fat_gt2tag_2017_mod.txt
    #    # 0fat_gt4tag: /eos/user/a/acarvalh/HHH/datacards_results/from_6loose_trees//HHH_6b_XGBoost_noFatJetVar_allBKG/datacard_0fat_gt4tag_2017_mod.txt
    #"HHH_6b_withFatJetVar_allBKG_correctBalance"               : {
    #    "fit_var" : "HHH_6b_withFatJetVar_allBKG_correctBalance/HHH_6b_XGB_onlyUnderstood_final_selection_jetMultiplicity_25Var",
    #    "binning" : [30, 0, 1]
    #    },
    "HHH_6b_v24"               : {
        "fit_var" : "HHH_6b_XGB_TMVA_Vars",
        "binning" : [30, 0, 1]
        },
    # /eos/user/a/acarvalh/HHH/BDT_results/HHH_6b_withFatJetVar_allBKG_correctBalance/HHH_6b_XGB_onlyUnderstood_final_selection_jetMultiplicity_25Var.pkl
    #    # gt2fat_gt2tag:
    #    # 2fat_gt2tag:
    #    # 1fat_gt2tag: doing
    #    # 0fat_gt4tag: doing


    # to do with jet selection
    # done with 0fat_gt4tag, 1fat_gt2tag
    #"XGBoost_jetMultSel_withFatJetVar" : "HHH_6b_withFatJetVar/HHH_6b_XGB_withFatJetVar_final_selection_jetMultiplicity_14Var*" # to do with jet selection AND with categories
    #"HT_BDTSel"       : "ht", # after BDT cut
}#options.histogram
bdts_postfit = ["pkl", "log"]
era          = "2017" #options.era
#shape        = options.shapeSyst

inputTree = 'Events'

# take procesess from config given the channel
processes = ["GluGluToHHHTo6B_SM", "BTagCSV", "QCD", "QCD6B", "TT", "WJetsToQQ", "WWTo4Q", "WWW", "WWZ", "WZZ", "ZJetsToQQ", "ZZTo4Q", "ZZZ"]
#processes = ["GluGluToHHHTo6B_SM"]
variables = ["eventWeightBTagCorrected"] #["xsecWeight", "l1PreFiringWeight", "puWeight", "weight", "genWeight"]
# load the other variables based in the .log files

step = 500000
for hist in histograms.keys() :

  folder_out_name = "%s/%s_%s" % (outputFolder, channel, hist)
  if not os.path.exists(folder_out_name):
      os.makedirs(folder_out_name)
  print("Created %s" % folder_out_name)

  # add variables to the list to take depending of the
  nbins = histograms[hist]["binning"][0]
  minX  = histograms[hist]["binning"][1]
  maxX  = histograms[hist]["binning"][2]
  to_compute = histograms[hist]["fit_var"]
  print("nbins, minX, maxX = ",nbins, minX, maxX)
  if not "XGB" in to_compute :
      variables += [to_compute]
      print("Will take the histogram %s " % to_compute)
  else :
    # take the variables from the log file
    # needs to be loaded ordered -- each python version oerder things diffently
    #my_file = open('%s/%s.log' % (bdts_base_dir, to_compute), 'r')
    my_file = open("/afs/cern.ch/work/a/acarvalh/HHH/mva_training_hhh_git/HHH_v24_MVAvar/HHH6B_XGB_asTMVA_final_selection_jetMultiplicity_62Var.log", 'r')

    bdt_var = my_file.read().replace("[","").replace("]","").replace("\n","").replace(" ","").replace("'","").split(",") # execfile('%s/%s.log' % (bdts_base_dir, to_compute))
    print("BDT variables :", bdt_var)
    heta_list=["h1_t3_eta", "h2_t3_eta", "h3_t3_eta"]
    for entry in bdt_var :
      print entry
    variables += bdt_var #+ heta_list
    #print(variables)

    pkl_read = "/afs/cern.ch/work/a/acarvalh/HHH/mva_training_hhh_git/HHH_v24_MVAvar/HHH6B_XGB_asTMVA_final_selection_jetMultiplicity_62Var.pkl" #'%s/%s.pkl' % (bdts_base_dir, to_compute)
    file = None
    try:
    	file = open(pkl_read,'rb')
    except IOError as e:
    	print('Couldnt open or write to file (%s).' % e)
    pkldata = pickle.load(file)
    print ('pkl loaded')
    # add also the etas to compute the additional variable
  #quit()

  for cat in categories :

    file_out_name = "%s/prepareDatacards_%s.root" % (folder_out_name, cat) if era == "none" else "%s/prepareDatacards_%s_%s.root" % (folder_out_name, cat, era)
    print("Creating %s" % file_out_name)

    if "6loose" in  cat  :
        folder_in_name = inputShapesOriginal
    else :
        folder_in_name = "%s/%s" % (inputShapes, cat) #inputShapes.replace("CAT",cat).replace("ERA",era)

    fileOut  = ROOT.TFile(file_out_name, "recreate")
    for pp, proc in enumerate(processes) :
        tlocal = time.localtime()
        current_time = time.strftime("%H:%M:%S", tlocal)
        print(current_time)
        seconds0 = time.time()
        proc_write = proc
        if proc == "BTagCSV" :
            proc_write = "data_obs"
        h2 = ROOT.TH1F(proc_write, proc_write, nbins, minX, maxX)
        file = "%s/%s.root" % (folder_in_name, proc) #file_in_name + "/histograms_" + proc + ".root"
        fin           = ROOT.TFile(file) #("%s/%s" % (odir, fileDatacard))
        tree = fin.Get(inputTree)
        # read by chuncks and go filling a histogram
        n_entries0 = tree.GetEntries()+1
        print("tree had %s entries" % str(n_entries0), file)
        stop_condition = step
        start_condition = 0
        sizeData = 10
        yield_total = 0
        lumi_2017 = 41480.0
        #data = pandas.DataFrame(columns=variables)
        while not sizeData == 0 : # n_entries0 :
            chunk_arr = tree2array(tree, start=start_condition, stop=stop_condition)
            sizeData = len(chunk_arr)
            print("filled from ", start_condition, stop_condition, sizeData)
            #print(variables)
            chunk_df = pandas.DataFrame(chunk_arr, columns=variables)
            stop_condition = stop_condition + step
            start_condition = start_condition + step
            chunk_df["totalWeight"] = chunk_df["eventWeight"]*bcand1LoosebtagFscaleSF*bcand1LoosebtagFscaleSF......... #lumi_2017 * chunk_df["xsecWeight"]  * chunk_df["l1PreFiringWeight"]  * chunk_df["puWeight"] * chunk_df["weight"] * chunk_df["genWeight"]
            #chunk_df["h1_t3_eta"]= abs(chunk_df["h1_t3_eta"])
            #chunk_df["h2_t3_eta"]= abs(chunk_df["h2_t3_eta"])
            #chunk_df["h3_t3_eta"]= abs(chunk_df["h3_t3_eta"])
            #chunk_df['max_h_eta']= chunk_df[["h1_t3_eta", "h2_t3_eta", "h3_t3_eta"]].max(axis=1)
            # do the additional variable before computing the BDT weights
            #weight * xsecWeight * l1PreFiringWeight * puWeight * genWeigh
            #print(chunk_df)
            h2_local = ROOT.TH1F(proc_write+"_toadd", proc_write, nbins, minX, maxX)

            if not "Var" in to_compute :
                var_toplot = chunk_df[to_compute]
            else :
                #print(chunk_df[bdt_var])
                var_toplot = pkldata.predict_proba(chunk_df[bdt_var].values)[:,1]
                #print(var_toplot)
                #quit()
                #sizeData = 0

                # compute the weights here
            #print(var_toplot)
            if proc_write == "data_obs" :
                fill_hist(h2_local, var_toplot)
            else :
                fill_hist(h2_local, var_toplot, weights=chunk_df["totalWeight"])
            yield_total = yield_total + h2_local.Integral()
            # add to the pandas instead
            #data=data.append(chunk_df, ignore_index=True)
            h2.Add(h2_local)

        fin.Close()
        fileOut.cd()
        print(proc_write, h2.Integral(), yield_total)
        h2.Write()
        seconds = time.time()
        print("Seconds, minutes to read and write : ", seconds-seconds0, (seconds-seconds0)/60.0)
    fileOut.Close()
    print("Wrote %s" % file_out_name)
    datacard_name = file_out_name.replace("prepareDatacards", "datacard").replace(".root", "_mod.txt")
    print("Wrote the datacard %s" % datacard_name)
    command = "python scripts/WriteDatacards.py --inputShapes %s --channel 6tag --cardFolder %s --noX_prefix" % (file_out_name, folder_out_name)
    print(command)

    proc=subprocess.Popen([command],shell=True,stdout=subprocess.PIPE)
    out = proc.stdout.read()

    # do fitdiag
    command = "cd combine_results ; combine -M FitDiagnostics      %s     --saveShapes     --saveWithUncertainties     --saveNormalization ; cd -" % datacard_name
    print(command)

    proc=subprocess.Popen([command],shell=True,stdout=subprocess.PIPE)
    out = proc.stdout.read()

    command = "python scripts/postfit_plots.py --plot_options_dict plot_configs/plot_options.json --output_folder combine_results/test_plots/ --overwrite_fitdiag combine_results/fitDiagnosticsTest.root --unblind"
    print(command)

    proc=subprocess.Popen([command],shell=True,stdout=subprocess.PIPE)
    out = proc.stdout.read()

    #for hist in histograms :
    #    histogram.Write("",TObject.kOverwrite)
    #    histo.Write("",TObject.kOverwrite)
    #    print ("histo.Write("",TObject.kOverwrite) :: histoName: ",histo.GetName())

"""
Add the following renamings for the BRs uncertainties application
GluGluToHHHTo6B_SM --> GluGluToHHH_SM_bbbbbb

"""
